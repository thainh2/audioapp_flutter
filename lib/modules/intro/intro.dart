import 'package:flutter/material.dart';

class Intro extends StatelessWidget {
  Intro();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 50),
              child: Image(
                  image: AssetImage('assets/image/grandmother.png'),
                  fit: BoxFit.fitWidth),
            ),
            Container(
              padding: EdgeInsets.only(top: 20),
              child: Image(
                  image: AssetImage('assets/image/fairy_tales_title.png'),
                  height: 42,
                  fit: BoxFit.fitHeight),
            ),
            Container(
              padding: EdgeInsets.only(top: 20),
              child: Column(
                children: <Widget>[
                  Container(
                    width: 320,
                    height: 44,
                    child: RaisedButton(
                        onPressed: () {},
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image(
                                image: AssetImage('assets/image/icon_apple.png'),
                                height: 15,
                                fit: BoxFit.fitHeight),
                            SizedBox(width: 10),
                            Text('Login with Apple',
                                style: TextStyle(
                                    fontSize: 20, color: Colors.white))
                          ],
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0)),
                        color: Colors.black),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: 320,
                    height: 44,
                    child: RaisedButton(
                        onPressed: () {},
                        child: const Text('Facebook',
                            style:
                            TextStyle(fontSize: 20, color: Colors.white)),
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0)),
                        color: Color.fromRGBO(30, 86, 42, 1)),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: 320,
                    height: 44,
                    child: RaisedButton(
                        onPressed: () {},
                        child: const Text('Google',
                            style: TextStyle(
                                fontSize: 20,
                                color: Color.fromRGBO(30, 86, 42, 1))),
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0),
                            side: BorderSide(
                                color: Color.fromRGBO(30, 86, 42, 1))),
                        color: Colors.white),
                  ),
                ],
              ),
            )
          ],
        ),
        Container(
          padding: EdgeInsets.only(bottom: 15),
          alignment: FractionalOffset.bottomCenter,
          child: Text.rich(TextSpan(children: <TextSpan>[
            TextSpan(text: 'Điều khoản', style: TextStyle(color: Colors.red)),
            TextSpan(text: ' người dùng và'),
            TextSpan(text: ' Bảo mật', style: TextStyle(color: Colors.red)),
            TextSpan(text: ' thông tin')
          ])),
        )
      ],
    );
  }
}
